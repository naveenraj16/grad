$(document).ready(function(){
    //populate advisor names
    $(this).load("http://tinman.cs.gsu.edu:8080/grad/advisors",function(responseTxt,statusTxt,xhr){
        var xmlDoc = $.parseXML(responseTxt);
        var $xml = $(xmlDoc);
        $xml.find("advisor").each(function(){
            var $aname = $(this).text();
            $("#advisors").append("<option value=\""+$aname+"\">"+$aname+"</option>");
        });
    });

    //populate years
    $(this).load("http://tinman.cs.gsu.edu:8080/grad/years",function(responseTxt,statusTxt,xhr){
        var xmlDoc = $.parseXML(responseTxt);
        var $xml = $(xmlDoc);
        $xml.find("year").each(function(){
            var $year = $(this).text();
            $("#years").append("<option value=\""+$year+"\">"+$year+"</option>");
        });
    });

    //react to advisor change
    $("#advisors").change(function() {
        var advisor = $("#advisors").val();
        var msphd = $("#msphd").val();
        var year = $("#years").val();
        $(this).getStudents(advisor,msphd,year);
    });

    //react to msphd change
    $("#msphd").change(function() {
        var advisor = $("#advisors").val();
        var msphd = $("#msphd").val();
        var year = $("#years").val();
        $(this).getStudents(advisor,msphd,year);
    });

    //react to year change
    $("#years").change(function() {
        var advisor = $("#advisors").val();
        var msphd = $("#msphd").val();
        var year = $("#years").val();
        $(this).getStudents(advisor,msphd,year);
    });

    $.fn.getStudents = function(advisor,msphd,year){
        var url = "http://tinman.cs.gsu.edu:8080/grad/"+advisor+"/"+msphd+"/";
        if(year != "all")
            url += year + "/";


        $(document).load(url,function(responseTxt,statusTxt,xhr){
            var xmlDoc = $.parseXML(responseTxt);
            var $xml = $(xmlDoc);
            var htmlString = "";
            if(msphd == "ms")
                htmlString = "<table border=\"2\"><tr><td>Name</td><td>Type</td><td>Month</td><td>Year</td></tr>";
            else
                htmlString = "<table border=\"2\"><tr><td>Name</td><td>Co-Advisor</td><td>Month</td><td>Year</td></tr>";
            $xml.find("student").each(function(){
                var sname = $(this).find("sname").text();
                var advisor = $(this).find("advisor").text();
                var type = "";
                if(msphd == "ms")
                    type = $(this).find("type").text();
                else
                    type = $(this).find("coadvisor").text();
                var month = $(this).find("month").text();
                if(month == "5")
                    month = "May";
                if(month == "8")
                    month = "August";
                if(month == "12")
                    month = "December";
                var year = $(this).find("year").text();

                htmlString += "<tr><td>"+sname+"</td><td>"+type+"</td><td>"+month+"</td><td>"+year+"</td></tr>"
            });
            htmlString += "</table>";

            $("#myDiv").html(htmlString);
        });
    }
});
