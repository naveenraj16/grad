/**
 * Created by Naveen on 5/31/15.
 */

var xmlhttp1;
var xmlhttp2;
var xmlhttp3;
var xmlhttp4;
var xmlhttp5;
var xmlhttp6;
function populateLists(){
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp1=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
    }

    var url = "http://tinman.cs.gsu.edu:8080/grad/advisors";
    xmlhttp1.open("GET",url,true);
    xmlhttp1.send();
    xmlhttp1.onreadystatechange=function(){
        if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
            document.getElementById("advisors").innerHTML+=createAdvisorSelectOptions(xmlhttp1.responseXML);
        }
        else{
            //alert(xmlhttp1.responseText);
        }
    };

    //get Years

    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp2=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
    }

    url = "http://tinman.cs.gsu.edu:8080/grad/years";
    xmlhttp2.open("GET",url,true);
    xmlhttp2.send();
    xmlhttp2.onreadystatechange=function(){
        if (xmlhttp2.readyState==4 && xmlhttp2.status==200){
            document.getElementById("years").innerHTML+=createYearsSelectOptions(xmlhttp2.responseXML);
        }
        else{
            //alert(xmlhttp2.responseText);
        }
    };

    //get masters count

    //document.getElementById("myDiv").innerHTML=
    createHTMLTable();

}

function createAdvisorSelectOptions(advisors){
    var optionString = "";
    var a = advisors.getElementsByTagName("advisor");
    for(i = 0; i< a.length; i++ ){
        optionString += "<option value=\""+ a.item(i).innerHTML+"\">"+ a.item(i).innerHTML+"</option>";
    }
    return optionString;
}

function createYearsSelectOptions(years){
    var optionString = "";
    var a = years.getElementsByTagName("year");
    for(i = 0; i< a.length; i++ ){
        optionString += "<option value=\""+ a.item(i).innerHTML+"\">"+ a.item(i).innerHTML+"</option>";
    }
    return optionString;
}

function createHTMLTable(){
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp3=new XMLHttpRequest();
    }
    else  {// code for IE6, IE5
        xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
    }

    var msCount = 0;
    var url1 = "http://tinman.cs.gsu.edu:8080/grad/ms/count";
    xmlhttp3.open("GET",url1,true);
    xmlhttp3.send();
    xmlhttp3.onreadystatechange=function(){
        if (xmlhttp3.readyState==4 && xmlhttp3.status==200){
            var e = xmlhttp3.responseXML.getElementsByTagName("count");
            msCount = e.item(0).innerHTML;
            if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp4=new XMLHttpRequest();
            }
            else  {// code for IE6, IE5
                xmlhttp4=new ActiveXObject("Microsoft.XMLHTTP");
            }
            var phdCount = 0;
            var url2 = "http://tinman.cs.gsu.edu:8080/grad/phd/count";
            xmlhttp4.open("GET",url2,true);
            xmlhttp4.send();
            xmlhttp4.onreadystatechange=function(){
                if (xmlhttp4.readyState==4 && xmlhttp4.status==200){
                    var e = xmlhttp4.responseXML.getElementsByTagName("count");
                    phdCount = e.item(0).innerHTML;
                    document.getElementById("myDiv").innerHTML =
                        "<table border=\"2\"><tr><th>Number of Ms Graduates</th><th>Number of PHD Graduates</th></tr>" +
                        "<tr><td>"+msCount+"</td><td>"+phdCount+"</td></tr></table>";
                }
                else{
                    //alert(xmlhttp4.responseText);
                }
            };
        }
        else{
            //alert(xmlhttp3.responseText);
        }
    };


    //alert(msCount);
    //alert(phdCount);
    document.getElementById("myDiv").innerHTML = "<h4>Something went wrong</h4>";
}


function getCount(){
    var e1 = document.getElementById("advisors");
    var advisor = e1.options[e1.selectedIndex].value;

    var e2 = document.getElementById("msphd");
    var msphd = e2.options[e2.selectedIndex].value;

    var e3 = document.getElementById("years");
    var year = e3.options[e3.selectedIndex].value;

    //alert(advisor);

    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp5=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp5=new ActiveXObject("Microsoft.XMLHTTP");
    }
    //alert(advisor);
    var url1 = "http://tinman.cs.gsu.edu:8080/grad/";
    var url2 = "http://tinman.cs.gsu.edu:8080/grad/";
    if(advisor != "all") {
        url1 += advisor + "/";
        url2 += advisor + "/";
    }
    if(msphd == "all") {
        url1 += "ms/";
        url2 += "phd/";
    }
    else
        url1 += msphd+"/";
    if(year != "all") {
        url1 += year+"/";
        url2 += year+"/";
    }

    url1 += "count";
    url2 += "count";

    //alert(url1);
    //alert(url2);
    if(msphd == "all") {
        xmlhttp5.open("GET", url1, true);
        //alert(url1);
        xmlhttp5.send();
        xmlhttp5.onreadystatechange = function () {
            if (xmlhttp5.readyState == 4 && xmlhttp5.status == 200) {
                var e = xmlhttp5.responseXML.getElementsByTagName("count");
                msCount = e.item(0).innerHTML;

                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp6 = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp6 = new ActiveXObject("Microsoft.XMLHTTP");
                }
                var phdCount = 0;
          //      alert(advisor);
                xmlhttp6.open("GET", url2, true);
                xmlhttp6.send();
                xmlhttp6.onreadystatechange = function () {
                    if (xmlhttp6.readyState == 4 && xmlhttp6.status == 200) {
                        var e = xmlhttp6.responseXML.getElementsByTagName("count");
                        phdCount = e.item(0).innerHTML;
                        document.getElementById("myDiv").innerHTML =
                            "<table border=\"2\"><tr><th>Number of Ms Graduates</th><th>Number of PHD Graduates</th></tr>" +
                            "<tr><td>" + msCount + "</td><td>" + phdCount + "</td></tr></table>";
                    }
                    else {
                        //alert(xmlhttp4.responseText);
                    }
                };

            }
            else {
                //alert(xmlhttp3.responseText);
            }
        };
    }//end of 'all' if
    else{
        var msCount = 0;
        var phdCount = 0;
        xmlhttp5.open("GET", url1, true);
        //alert(url1);
        xmlhttp5.send();
        xmlhttp5.onreadystatechange = function () {
            if (xmlhttp5.readyState == 4 && xmlhttp5.status == 200) {
                var e = xmlhttp5.responseXML.getElementsByTagName("count");
                if (msphd == "ms")
                    msCount = e.item(0).innerHTML;
                else
                    phdCount = e.item(0).innerHTML;
                document.getElementById("myDiv").innerHTML =
                    "<table border=\"2\"><tr><th>Number of Ms Graduates</th><th>Number of PHD Graduates</th></tr>" +
                    "<tr><td>" + msCount + "</td><td>" + phdCount + "</td></tr></table>";
            }
            else {
                //alert(xmlhttp4.responseText);
            }
        };
    }
}
