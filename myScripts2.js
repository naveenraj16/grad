/**
 * Created by Naveen on 6/14/15.
 */
/**
 * Created by Naveen on 5/31/15.
 */

var xmlhttp1;
var xmlhttp2;
var xmlhttp3;
var xmlhttp4;
var xmlhttp5;
var xmlhttp6;
function populateLists(){
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp1=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
    }

    var url = "http://tinman.cs.gsu.edu:8080/grad/advisors";
    xmlhttp1.open("GET",url,true);
    xmlhttp1.send();
    xmlhttp1.onreadystatechange=function(){
        if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
            document.getElementById("advisors").innerHTML+=createAdvisorSelectOptions(xmlhttp1.responseXML);
        }
        else{
            //alert(xmlhttp1.responseText);
        }
    };

    //get Years

    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp2=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
    }

    url = "http://tinman.cs.gsu.edu:8080/grad/years";
    xmlhttp2.open("GET",url,true);
    xmlhttp2.send();
    xmlhttp2.onreadystatechange=function(){
        if (xmlhttp2.readyState==4 && xmlhttp2.status==200){
            document.getElementById("years").innerHTML+=createYearsSelectOptions(xmlhttp2.responseXML);
        }
        else{
            //alert(xmlhttp2.responseText);
        }
    };
    getNames();

}

function createAdvisorSelectOptions(advisors){
    var optionString = "";
    var a = advisors.getElementsByTagName("advisor");
    for(i = 0; i< a.length; i++ ){
        optionString += "<option value=\""+ a.item(i).innerHTML+"\">"+ a.item(i).innerHTML+"</option>";
    }
    return optionString;
}

function createYearsSelectOptions(years){
    var optionString = "";
    var a = years.getElementsByTagName("year");
    for(i = 0; i< a.length; i++ ){
        optionString += "<option value=\""+ a.item(i).innerHTML+"\">"+ a.item(i).innerHTML+"</option>";
    }
    return optionString;
}


function getNames(){
    var e1 = document.getElementById("advisors");
    var advisor = e1.options[e1.selectedIndex].value;

    var e2 = document.getElementById("msphd");
    var msphd = e2.options[e2.selectedIndex].value;

    var e3 = document.getElementById("years");
    var year = e3.options[e3.selectedIndex].value;

    //alert(advisor);

    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp5=new XMLHttpRequest();
    }

    else  {// code for IE6, IE5
        xmlhttp5=new ActiveXObject("Microsoft.XMLHTTP");
    }
    //alert(advisor);
    var url1 = "http://tinman.cs.gsu.edu:8080/grad/";
    url1 += advisor + "/";
    url1 += msphd+"/";
    if(year != "all")
        url1 += year + "/";


    //alert(url1);
    //alert(url2);
        var htmlString = "";
        if(msphd == "ms")
            htmlString = "<table border=\"2\"><tr><th>Name</th><th>Type</th><th>Month</th><th>Year</th></tr>";
        else
            htmlString = "<table border=\"2\"><tr><th>Name</th><th>Co-Advisor</th><th>Month</th><th>Year</th></tr>";
        xmlhttp5.open("GET", url1, true);
        //alert(url1);
        xmlhttp5.send();
        xmlhttp5.onreadystatechange = function () {
            if (xmlhttp5.readyState == 4 && xmlhttp5.status == 200) {
                var e = xmlhttp5.responseXML.getElementsByTagName("student");
                for(var i = 0; i < e.length; i++){
                    var student = e.item(i);
                    var name = student.getElementsByTagName("sname").item(0).innerHTML;
                    if(msphd == "ms")
                        var type = student.getElementsByTagName("type").item(0).innerHTML;
                    else
                        var type = student.getElementsByTagName("coadvisor").item(0).innerHTML;
                    var month = student.getElementsByTagName("month").item(0).innerHTML;
                    var year = student.getElementsByTagName("year").item(0).innerHTML;
                    if(month == "5")
                        month = "May"
                    if(month == "8")
                        month = "August"
                    if(month == "12")
                        month = "December"
                    htmlString += "<tr><td>"+name+"</td><td>"+type+"</td><td>"+month+"</td><td>"+year+"</td></tr>"
                }
                htmlString += "</table>";
                document.getElementById("myDiv").innerHTML = htmlString;

            }

        };


}
